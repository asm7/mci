## Architecture

```mermaid
%%{init: { 'theme': 'default' } }%%
graph LR
classDef cluster fill:#F2ECE8,stroke:#333,stroke-width:1px;
classDef namespace fill:#99C4C8,color:#fff,stroke:#333,stroke-width:1px;
classDef pod fill:#C3E5E9,stroke:#333,stroke-width:1px;
classDef anthos fill:#E7ECEF,stroke:#333,stroke-width:1px;

subgraph GCLB
  mci
end

subgraph gke-west
  subgraph westistio[istio-system namespace]
    subgraph westistioigw[istio-ingressgateway Pod]
    end
  end
  subgraph westworldwest[world-west namespace]
    subgraph westworldwestpod[world-west Pod]
    end
  end
  subgraph westkubewest[kube-west namespace]
    subgraph westkubewestpod[kube-west Pod]
    end
  end
  mci --> |/west| westistioigw
  westistioigw -->|/west/world| westworldwestpod
  westistioigw -->|/west/kube| westkubewestpod
end

subgraph gke-central
  subgraph centralistio[istio-system namespace]
    subgraph centralistioigw[istio-ingressgateway Pod]
    end
  end
  subgraph centralworldcentral[world-central namespace]
    subgraph centralworldcentralpod[world-central Pod]
    end
  end
  subgraph centralkubecentral[kube-central namespace]
    subgraph centralkubecentralpod[kube-central Pod]
    end
  end

  mci --> |/central| centralistioigw
  centralistioigw -->|/central/world| centralworldcentralpod
  centralistioigw -->|/central/kube| centralkubecentralpod

end

class gke-west,gke-central cluster;
class westistio,westworldwest,westkubewest,centralistio,centralworldcentral,centralkubecentral namespace;
class centralistioigw,westistioigw,westworldwestpod,westkubewestpod,centralworldcentralpod,centralkubecentralpod pod;

```

# ASM 1.8 with single MCI and multiple MCS

---

# Environment

1.  Define variables.

```
    # Enter your project ID below
    export PROJECT_ID=YOUR PROJECT ID HERE

    # Copy paste the rest below
    export PROJECT_NUM=$(gcloud projects describe ${PROJECT_ID} --format='value(projectNumber)')
    export CLUSTER_1=gke-west
    export CLUSTER_2=gke-central
    export CLUSTER_1_ZONE=us-west2-a
    export CLUSTER_2_ZONE=us-central1-a
    export CLUSTER_INGRESS=gke-ingress
    export CLUSTER_INGRESS_ZONE=us-west1-a
    export WORKLOAD_POOL=${PROJECT_ID}.svc.id.goog
    export MESH_ID="proj-${PROJECT_NUM}"
    export ASM_VERSION=1.8
    export ISTIO_VERSION=1.8.1-asm.5
    export ASM_LABEL=asm-181-5
```

# Tooling

1.  Install krew. Learn more at https://krew.sigs.k8s.io/.

```
    (
    set -x; cd "$(mktemp -d)" &&
    curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/krew.{tar.gz,yaml}" &&
    tar zxvf krew.tar.gz &&
    KREW=./krew-"$(uname | tr '[:upper:]' '[:lower:]')_amd64" &&
    $KREW install --manifest=krew.yaml --archive=krew.tar.gz &&
    $KREW update
    )
    echo -e "export PATH="${PATH}:${HOME}/.krew/bin"" >> ~/.bashrc && source ~/.bashrc
```

1.  Install ctx and ns via krew for easy context switching.

```
    kubectl krew install ctx
    kubectl krew install ns
```

# Enable APIs

1.  Enable the required APIs

```
    gcloud services enable \
    --project=${PROJECT_ID} \
    anthos.googleapis.com \
    container.googleapis.com \
    compute.googleapis.com \
    monitoring.googleapis.com \
    logging.googleapis.com \
    cloudtrace.googleapis.com \
    meshca.googleapis.com \
    meshtelemetry.googleapis.com \
    meshconfig.googleapis.com \
    iamcredentials.googleapis.com \
    gkeconnect.googleapis.com \
    gkehub.googleapis.com \
    cloudresourcemanager.googleapis.com
```

# Create GKE clusters

1.  Create two GKE clusters.

```
    gcloud beta container clusters create ${CLUSTER_1} \
    --project ${PROJECT_ID} \
    --zone=${CLUSTER_1_ZONE} \
    --machine-type "e2-standard-4" \
    --num-nodes "3" --min-nodes "3" --max-nodes "5" \
    --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
    --workload-pool=${WORKLOAD_POOL} \
    --labels=mesh_id=${MESH_ID} --async

    gcloud beta container clusters create ${CLUSTER_2} \
    --project ${PROJECT_ID} \
    --zone=${CLUSTER_2_ZONE} \
    --machine-type "e2-standard-4" \
    --num-nodes "3" --min-nodes "3" --max-nodes "5" \
    --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
    --workload-pool=${WORKLOAD_POOL} \
    --labels=mesh_id=${MESH_ID}
```

1.  Confirm clusters are `RUNNING`.

```
    gcloud container clusters list

    Output (do not copy)

    NAME         LOCATION       MASTER_VERSION   MASTER_IP      MACHINE_TYPE   NODE_VERSION     NUM_NODES  STATUS
    gke-central  us-central1-a  1.16.13-gke.401  34.123.79.68   e2-standard-4  1.16.13-gke.401  3          RUNNING
    gke-west     us-west2-a     1.16.13-gke.401  35.236.25.123  e2-standard-4  1.16.13-gke.401  3          RUNNING
```

1.  Connect to clusters.

```
    touch ~/asm-kubeconfig && export KUBECONFIG=~/asm-kubeconfig
    gcloud container clusters get-credentials ${CLUSTER_1} --zone ${CLUSTER_1_ZONE}
    gcloud container clusters get-credentials ${CLUSTER_2} --zone ${CLUSTER_2_ZONE}
```

> Remember to unset your `KUBECONFIG` var at the end.

1.  Rename cluster context for easy switching.

```
    kubectl ctx ${CLUSTER_1}=gke_${PROJECT_ID}_${CLUSTER_1_ZONE}_${CLUSTER_1}
    kubectl ctx ${CLUSTER_2}=gke_${PROJECT_ID}_${CLUSTER_2_ZONE}_${CLUSTER_2}
```

1.  Confirm both cluster contextx are present.

```
    kubectl ctx

    Output (do not copy)

    gke-central
    gke-west
```

# Install ASM

1.  Download the version of the script that installs Anthos Service Mesh 1.8.1 to the current working directory.

```
    curl https://storage.googleapis.com/csm-artifacts/asm/install_asm_"${ASM_VERSION}" > install_asm
    chmod +x install_asm
```

1.  Create an `asm` folder to keep the `istioctl` CLI utility and add it to your $PATH.

```
    mkdir -p ${HOME}/asm-${ASM_VERSION} && export PATH=$PATH:$HOME/asm-${ASM_VERSION}
```

1.  Install ASM on both clusters using the `install_asm.sh` script.

```
    ./install_asm \
    --project_id ${PROJECT_ID} \
    --cluster_name ${CLUSTER_1} \
    --cluster_location ${CLUSTER_1_ZONE} \
    --mode install \
    --output_dir ${HOME}/asm-${ASM_VERSION} \
    --enable_all

    ./install_asm \
    --project_id ${PROJECT_ID} \
    --cluster_name ${CLUSTER_2} \
    --cluster_location ${CLUSTER_2_ZONE} \
    --mode install \
    --enable_all

    Output (do not copy)

    ...
    install_asm: Successfully installed ASM.
```

# Cross cluster service discovery

1.  Create a secret with the CLUSTER_1 kubeconfig and deploy to CLUSTER_2.

```
    istioctl x create-remote-secret \
    --context=${CLUSTER_1} \
    --name=${CLUSTER_1} > secret-kubeconfig-${CLUSTER_1}.yaml
    kubectl --context=${CLUSTER_2} -n istio-system apply -f secret-kubeconfig-${CLUSTER_1}.yaml
```

1.  Create a secret with the CLUSTER_2 kubeconfig and deploy to CLUSTER_1.

```
    istioctl x create-remote-secret \
    --context=${CLUSTER_2} \
    --name=${CLUSTER_2} > secret-kubeconfig-${CLUSTER_2}.yaml
    kubectl --context=${CLUSTER_1} -n istio-system apply -f secret-kubeconfig-${CLUSTER_2}.yaml
```

# Ingress for Anthos

1.  Ingress for Anthos config cluster create

```
    gcloud services enable anthos.googleapis.com gkehub.googleapis.com multiclusteringress.googleapis.com

    gcloud container clusters create ${CLUSTER_INGRESS} --zone ${CLUSTER_INGRESS_ZONE} --num-nodes=3 --enable-ip-alias

    gcloud container clusters get-credentials ${CLUSTER_INGRESS} --zone ${CLUSTER_INGRESS_ZONE} --project ${PROJECT_ID}
    
    kubectl ctx ${CLUSTER_INGRESS}=gke_${PROJECT_ID}_${CLUSTER_INGRESS_ZONE}_${CLUSTER_INGRESS}
```

1.  Register all clusters to hub.

```
    gcloud iam service-accounts create ingress-svc-acct

    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:ingress-svc-acct@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/gkehub.connect"

    gcloud iam service-accounts keys \
    create ~/ingress-svc-acct.json \
    --iam-account=ingress-svc-acct@${PROJECT_ID}.iam.gserviceaccount.com

    export INGRESS_CONFIG_URI=$(gcloud container clusters list --uri | grep ${CLUSTER_INGRESS})
    export CLUSTER_1_URI=$(gcloud container clusters list --uri | grep ${CLUSTER_1})
    export CLUSTER_2_URI=$(gcloud container clusters list --uri | grep ${CLUSTER_2})

    gcloud container hub memberships register ${CLUSTER_INGRESS} \
    --project=${PROJECT_ID} \
    --gke-uri=${INGRESS_CONFIG_URI} \
    --service-account-key-file=${HOME}/ingress-svc-acct.json

    gcloud container hub memberships register ${CLUSTER_1} \
    --project=${PROJECT_ID} \
    --gke-uri=${CLUSTER_1_URI} \
    --service-account-key-file=${HOME}/ingress-svc-acct.json

    gcloud container hub memberships register ${CLUSTER_2} \
    --project=${PROJECT_ID} \
    --gke-uri=${CLUSTER_2_URI} \
    --service-account-key-file=${HOME}/ingress-svc-acct.json

    gcloud container hub memberships list

    Output (do not copy)

    NAME            EXTERNAL_ID
    gke-west        7fe5b7ce-50d0-4e64-a9af-55d37b3dd3fa
    gke-central     6f1f6bb2-a3f6-4e9c-be52-6907d9d258cd
    gke-ingress     3574ee0f-b7e6-11ea-9787-42010a8a019c
```

1.  Enable MCI in the CLUSTER_INGRESS

```
    gcloud alpha container hub ingress enable \
    --config-membership=projects/${PROJECT_ID}/locations/global/memberships/${CLUSTER_INGRESS}

    gcloud alpha container hub ingress describe

    Output (do not copy)

    featureState:
      details:
        code: OK
        description: Ready to use
      detailsByMembership:
        projects/837232410721/locations/global/memberships/gke-central:
          code: OK
        projects/837232410721/locations/global/memberships/gke-west:
          code: OK
        projects/837232410721/locations/global/memberships/ingress:
          code: OK
      lifecycleState: ENABLED
```

1.  Verify that the two CRDs (MCS and MCI) have been deployed in the CLUSTER_INGRESS.

```
    kubectl --context=${CLUSTER_INGRESS} get crd | grep multicluster

    Output (do not copy)

    multiclusteringresses.networking.gke.io     2020-10-29T17:32:50Z
    multiclusterservices.networking.gke.io      2020-10-29T17:32:50Z
```

# Deploy Services

1.  Deploy services on central and west clusters.

```
    git clone https://gitlab.com/asm7/mci.git $HOME/mci
    kubectl --context ${CLUSTER_1} apply -k $HOME/mci/west/
    kubectl --context ${CLUSTER_2} apply -k $HOME/mci/central/
```

1.  Check virtualservices are configured properly.

```
    export CENTRAL_IGW_IP=$(kubectl --context ${CLUSTER_2} --namespace istio-system get svc istio-ingressgateway -o jsonpath={.status.loadBalancer.ingress..ip})
    export WEST_IGW_IP=$(kubectl --context ${CLUSTER_1} --namespace istio-system get svc istio-ingressgateway -o jsonpath={.status.loadBalancer.ingress..ip})

    curl $CENTRAL_IGW_IP/central/world

    Output (do not copy)

    Hello, world!
    Version: 2.0.0
    Hostname: hello-world-deployment-68c58989bb-z5rpn

    curl $CENTRAL_IGW_IP/central/kube

    Output (do not copy)

    Hello Kubernetes!

    curl $WEST_IGW_IP/west/world

    Output (do not copy)

    "Hello, world!
    Version: 2.0.0
    Hostname: hello-world-deployment-68c58989bb-z5rpn"

    curl $WEST_IGW_IP/west/kube

    Output (do not copy)

    Hello Kubernetes!
```

1.  Deploy MCI and MCS

```
    kubectl --context ${CLUSTER_INGRESS} apply -k $HOME/mci/ingress/
```

1.  Wait until you get a GCLB IP.

```
    watch kubectl --context ${CLUSTER_INGRESS} -n istio-system get multiclusteringress -o jsonpath="{.items[].status.VIP}"
```

1.  Test all four services using the GCLB IP.

```
    export GCLB_IP=$(kubectl --context ${CLUSTER_INGRESS} -n istio-system get multiclusteringress -o jsonpath="{.items[].status.VIP}")
    curl $GCLB_IP/central/world
    curl $GCLB_IP/central/kube
    curl $GCLB_IP/west/world
    curl $GCLB_IP/west/kube
```

> If you get a `404` (and then a `502`) when you navigate to the GCLB IP, just wait a few minutes longer.


